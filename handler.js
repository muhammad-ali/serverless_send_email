'use strict';

const AWS = require('aws-sdk');
const SES = new AWS.SES({region: 'eu-west-1'});
const Headers =  {
    'Content-Type': 'application/json',
    "Access-Control-Allow-Headers" : "Content-Type",
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Methods': 'POST, OPTIONS',
  }

const  sendEmail = formData => {
  //Destructuring Variables
  const { email, message, first_name, last_name, subject } = formData;
  //Email Body formation
  const emailParams = {
    Source: 'ali@dairatech.com',
    ReplyToAddresses: [email],
    Destination: {
      ToAddresses: ['ali@dairatech.com','nu.muhammad.ali@gmail.com'],
    },
    Message: {
      Body: {
        Text: {
          Charset: 'UTF-8',
          Data: `${message}\n\nName: ${first_name} ${last_name}\nEmail: ${email}`,
        },
      },
      Subject: {
        Charset: 'UTF-8',
        Data: `New message from dairatech.com | ${subject}`,
      },
    },
  };
  //Dispatching Email
  const sendPromise = SES.sendEmail(emailParams).promise();
  const response  = sendPromise.then(
  function(data) {
    const promiseResponse = {
      statusCode: 200,
      headers: Headers,
      body: JSON.stringify(data)
    }
    return promiseResponse;
  }).catch(
    function(err) {
    console.error(err, err.stack);
  });

  // returning promise
  return response;
}



module.exports.staticSiteMailer = async function (event) {
  const response =  await sendEmail(JSON.parse(event.body));
  return response;
};
